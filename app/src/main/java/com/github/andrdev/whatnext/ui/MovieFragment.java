package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Song;


public class MovieFragment extends BaseListFragment<Song> {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Movie mCurrentMovie = getActivity().getIntent().getBundleExtra(Constants.BUNDLE).getParcelable(Constants.CURRENT_MOVIE);
        getBundle().putString(Constants.CURRENT_MOVIE, mCurrentMovie.getId());
    }

    @Override
    int getDataType() {
        return DataTypesConst.SONGS;
    }

    @Override
    int getLayout() {
        return R.layout.fragment_rv;
    }

    @Override
    void onRowClick(View view, int position) {

    }
}
