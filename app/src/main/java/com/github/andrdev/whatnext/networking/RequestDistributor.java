package com.github.andrdev.whatnext.networking;


import android.util.Log;

import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.networking.NetworkRequests.EpisodesRequest;
import com.github.andrdev.whatnext.networking.NetworkRequests.MoviesRequest;
import com.github.andrdev.whatnext.networking.NetworkRequests.SeasonsRequest;
import com.github.andrdev.whatnext.networking.NetworkRequests.ShowsRequest;
import com.github.andrdev.whatnext.networking.NetworkRequests.SongsMovieRequest;
import com.github.andrdev.whatnext.networking.NetworkRequests.SongsShowRequest;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

public class RequestDistributor {

    public static RetrofitSpiceRequest getRequest(int requestType, String...args) {
        switch (requestType) {
            case DataTypesConst.MOVIES:
                return new MoviesRequest();
            case DataTypesConst.SHOWS:
                return new ShowsRequest();
            case DataTypesConst.SEASONS:
                return new SeasonsRequest(args[0]);
            case DataTypesConst.EPISODES:
                return new EpisodesRequest(args[0], args[1]);
            case DataTypesConst.SONGS:
                if (args.length == 3) {
                    return new SongsShowRequest(args[0], args[1], args[2]);
                } else {
                    return new SongsMovieRequest(args[0]);
                }
            default:
                return null;
        }
    }
}
