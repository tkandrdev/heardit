package com.github.andrdev.whatnext.animation;

import android.view.View;


// stores data for toolbar on scroll animation
public class MovingToolbarAnimationBag {

    //view to move and resize
    private View mResizeView;

    //container to move
    private View mMovingContainer;

    //move by px
    private int mMoveBy;

    private int mResizeViewNewTop;

    private int mResizeViewNewHeight;

    private int mMovingContainerNewTop;

    public MovingToolbarAnimationBag(View resizeView, View movingContainer, int moveBy) {
        mResizeView = resizeView;
        mMovingContainer = movingContainer;
        mMoveBy = moveBy;
        mResizeViewNewHeight = mResizeView.getHeight() + mMoveBy;
        calcMovingContainerNewTop();
        calcResizeViewNewTop();
    }

    View getResizeView() {
        return mResizeView;
    }

    void setResizeView(View resizeView) {
        this.mResizeView = resizeView;
    }

    View getMovingContainer() {
        return mMovingContainer;
    }

    void setMovingContainer(View movingContainer) {
        this.mMovingContainer = movingContainer;
    }

    int getMoveBy() {
        return mMoveBy;
    }

    void setMoveBy(int moveBy) {
        this.mMoveBy = moveBy;
    }

    int getResizeViewNewTop() {
        return mResizeViewNewTop;
    }

    int getResizeViewOldTop() {
        return mResizeView.getTop();
    }

    int getResizeViewHeight() {
        return mResizeView.getHeight();
    }

    int getResizeViewNewHeight() {
        return mResizeViewNewHeight;
    }

    //sets height for resizeView and calls requestLayout
    public void setResizeViewHeight(int newHeight) {
        mResizeView.getLayoutParams().height = newHeight;
        mResizeView.requestLayout();
    }

    int getMovingContainerNewTop() {
        return mMovingContainerNewTop;
    }

    int getMovingContainerOldTop() {
        return mMovingContainer.getTop();
    }

    private void calcMovingContainerNewTop() {
        mMovingContainerNewTop = mMovingContainer.getTop() - mMoveBy;
    }

    private void calcResizeViewNewTop() {
        mResizeViewNewTop = mResizeView.getTop() - mMoveBy;
    }

    boolean isContainerDown() {
        return mMovingContainer.getY() == mMovingContainer.getTop();
    }
}
