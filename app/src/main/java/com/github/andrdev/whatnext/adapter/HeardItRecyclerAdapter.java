package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.HeardItData;

import java.util.List;


public class HeardItRecyclerAdapter<T extends HeardItData>
        extends RecyclerView.Adapter<BasicViewHolder<T>> {


    private final LayoutInflater mLayoutInflater;
    private final List<T> mDataList;
    private final int mViewHolderType;

    public HeardItRecyclerAdapter
            (Context context, List<T> list, int viewHolderType) {
        mLayoutInflater = LayoutInflater.from(context);
        mDataList = list;
        this.mViewHolderType = viewHolderType;
    }


    @Override
    public BasicViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        BasicViewHolder holder;
        switch (mViewHolderType) {
            case DataTypesConst.MOVIES:
                view = mLayoutInflater.inflate(R.layout.card_movie, parent, false);
                holder = new MoviesRecyclerViewHolder(view);
                break;
            case DataTypesConst.SHOWS:
                view = mLayoutInflater.inflate(R.layout.card_show, parent, false);
                holder = new ShowsRecyclerViewHolder(view);
                break;
            case DataTypesConst.SEASONS:
                view = mLayoutInflater.inflate(R.layout.row_season, parent, false);
                holder = new SeasonsRecyclerViewHolder(view);
                break;
            case DataTypesConst.EPISODES:
                view = mLayoutInflater.inflate(R.layout.row_episode, parent, false);
                holder = new EpisodesRecyclerViewHolder(view);
                break;
            case DataTypesConst.SONGS:
                view = mLayoutInflater.inflate(R.layout.row_song, parent, false);
                holder = new SongsRecyclerViewHolder(view);
                break;
            default:
                return null;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(BasicViewHolder<T> holder, int position) {
        holder.bindView(mLayoutInflater.getContext(), mDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }


}
