package com.github.andrdev.whatnext.adapter;

import android.support.v4.app.Fragment;

import com.github.andrdev.whatnext.ui.MoviesListFragment;
import com.github.andrdev.whatnext.ui.ShowsListFragment;


public class MainTabHelper extends TabHelper {

    private final Fragment[] mFragments = {new MoviesListFragment(), new ShowsListFragment()};
    private final static String[] TITLES = {"MOVIES", "TV'S"};

    @Override
    int getCount() {
        return TITLES.length;
    }

    @Override
    Fragment getFragment(int position) {
        return mFragments[position];
    }

    @Override
    String getTitle(int position) {
        return TITLES[position];
    }
}
