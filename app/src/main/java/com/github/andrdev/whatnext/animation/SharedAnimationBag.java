package com.github.andrdev.whatnext.animation;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


//stores animation type and sharedViews parameters from the previous activity
public class SharedAnimationBag implements Parcelable {

    //type of animation. currently only SHARED_ANIMATION available
    private int mAnimationType;

    //info about shared views from old activity
    private Map<String, SharedViewOldParameters> mAnimationData;


    public SharedAnimationBag() {
        mAnimationData = new HashMap<>();
    }

    public SharedAnimationBag(int type, View... views) {
        this();
        mAnimationType = type;
        for (View v : views) {
            int[] arr = new int[2];
            v.getLocationOnScreen(arr);
            SharedViewOldParameters viewParameters = new SharedViewOldParameters();
            viewParameters.setLeft(arr[0]);
            viewParameters.setTop(arr[1]);
            viewParameters.setHeight(v.getHeight());
            viewParameters.setWidth(v.getWidth());
            mAnimationData.put((String) v.getTag(), viewParameters);
        }

    }

    public void setAnimationType(int mAnimationType) {
        this.mAnimationType = mAnimationType;
    }

    public void addAnimationData(String tag, SharedViewOldParameters sharedViewOldParameters) {
        mAnimationData.put(tag, sharedViewOldParameters);
    }


    Map<String, SharedViewOldParameters> getAnimationData() {
        return mAnimationData;
    }

    public int getAnimationType() {
        return mAnimationType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mAnimationType);
        Bundle bundle = new Bundle();
        Set<String> keySet = mAnimationData.keySet();
        for (String key : keySet) {
            bundle.putParcelable(key, mAnimationData.get(key));
        }
        dest.writeStringArray(keySet.toArray(new String[keySet.size()]));
        dest.writeBundle(bundle);
    }

    public static final Parcelable.Creator<SharedAnimationBag> CREATOR = new Parcelable.Creator<SharedAnimationBag>() {

        public SharedAnimationBag createFromParcel(Parcel in) {
            return new SharedAnimationBag(in);
        }

        public SharedAnimationBag[] newArray(int size) {
            return new SharedAnimationBag[size];
        }

    };

    private SharedAnimationBag(Parcel in) {
        this();
        mAnimationType = in.readInt();
        String[] keys = in.createStringArray();
        Bundle bundle = in.readBundle(SharedViewOldParameters.class.getClassLoader());
        for (String key : keys)
            mAnimationData.put(key, bundle.getParcelable(key));
    }

}
