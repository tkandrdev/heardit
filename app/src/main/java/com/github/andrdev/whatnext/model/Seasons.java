package com.github.andrdev.whatnext.model;

import com.google.gson.annotations.Expose;

import java.util.List;


public class Seasons implements HeardItBulkData<Season> {

    @Expose
    private List<Season> seasons;

    @Override
    public List<Season> getList() {
        return seasons;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seasons seasons1 = (Seasons) o;

        if (seasons != null ? !seasons.equals(seasons1.seasons) : seasons1.seasons != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return seasons != null ? seasons.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Seasons{" +
                "seasons=" + seasons +
                '}';
    }
}
