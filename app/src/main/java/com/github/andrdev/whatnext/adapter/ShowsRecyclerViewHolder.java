package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.ui.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ShowsRecyclerViewHolder extends BasicViewHolder<Show> {

    @InjectView(R.id.poster)
     ImageView mPoster;
    @InjectView(R.id.title)
     TextView mTitle;
    @InjectView(R.id.circularProgressBar)
    ProgressBar progressBar;
    Callback callback = new Callback() {
        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onError() {
            progressBar.setVisibility(View.INVISIBLE);
            mPoster.setImageResource(R.drawable.ic_launcher);
        }
    };
    public ShowsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    @Override
    void bindView(Context context, Show item) {
        progressBar.setVisibility(View.VISIBLE);
        Picasso.with(context)
                .load("http:" + item.getThumb_url())
                .transform(new RoundedTransformation(4, 0)).fit().into(mPoster, callback);
        mTitle.setText(item.getName());
    }
}