package com.github.andrdev.whatnext.networking;


import com.github.andrdev.whatnext.model.Episodes;
import com.github.andrdev.whatnext.model.HeardItBulkData;
import com.github.andrdev.whatnext.model.Movies;
import com.github.andrdev.whatnext.model.Seasons;
import com.github.andrdev.whatnext.model.Shows;
import com.github.andrdev.whatnext.model.Songs;

import retrofit.http.GET;
import retrofit.http.Path;


//api calls service for retrofit
public interface TuneFindService {

    @GET("/movie")
    Movies movies();

    @GET("/show")
    Shows shows();

    @GET("/show/{show}")
    Seasons seasons(@Path("show") String season);

    @GET("/show/{show}/season-{season}")
    Episodes episodes(@Path("show")String show, @Path("season") String season);

    @GET("/show/{show}/season-{season}/{episode}")
    Songs showSongs(@Path("show")String show, @Path("season") String season, @Path("episode") String episode);

    @GET("/movie/{movie}")
    Songs movieSongs(@Path("movie") String movie);

}
