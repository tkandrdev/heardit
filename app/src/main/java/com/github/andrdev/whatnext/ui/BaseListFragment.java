package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.adapter.HeardItRecyclerAdapter;
import com.github.andrdev.whatnext.adapter.RecyclerItemClickListener;
import com.github.andrdev.whatnext.adapter.ScrollListener;
import com.github.andrdev.whatnext.model.HeardItData;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.view.AutofitRecyclerView;
import com.github.andrdev.whatnext.view.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


abstract class BaseListFragment<T extends HeardItData & Parcelable> extends Fragment
        implements OnLoadListener<T> {

    @InjectView(R.id.recyclerView)
     RecyclerView mRecyclerView;
    private HeardItRecyclerAdapter<T> mRecyclerAdapter;
    private List<T> mDataList;
    private Bundle mBundle;
    ActivityCallback mActivityCallback;
    boolean showDivider = true;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mDataList = new ArrayList<>();
        mBundle = getArguments();
        if (mBundle == null) {
            mBundle = new Bundle();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        mActivityCallback = (ActivityCallback) getActivity();
        ButterKnife.inject(this, view);
        initViews();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (sendOnStartRequest()) {
            mActivityCallback.sendRequest(getDataType(), this, mBundle);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    void initViews() {
        if(!(mRecyclerView instanceof AutofitRecyclerView)) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), this::onRowClick));
        //set row dividers, if needed
        if(showDivider) {
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.line_divider)));
        }
        mRecyclerView.setOnScrollListener(
                new ScrollListener((LinearLayoutManager)mRecyclerView.getLayoutManager(), 0, mActivityCallback));
        mRecyclerAdapter =
                new HeardItRecyclerAdapter<>(getActivity(), mDataList, getDataType());
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }

    //call with true if horizontal dividers needed
    void showDivider(boolean show){
        showDivider = show;
    }

    @Override
    public void dataLoad(List<T> list) {
        mDataList.clear();
        mDataList.addAll(list);
        mRecyclerAdapter.notifyDataSetChanged();
        mRecyclerView.smoothScrollToPosition(0);
    }

    //type of needed data
    abstract int getDataType();

    //layout id
    abstract int getLayout();

    //action on rowclick
    abstract void onRowClick(View view, int position);

    Bundle getBundle(){
        return mBundle;
    }

    ActivityCallback getActivityCallback() {
        return mActivityCallback;
    }

    List<T> getDataList() {
        return mDataList;
    }

    //true if there is a need to send API request from onstart
    boolean sendOnStartRequest() {
        return mDataList.isEmpty();
    }

    void setData(List<T> list){
        mDataList = list;
    }
}
