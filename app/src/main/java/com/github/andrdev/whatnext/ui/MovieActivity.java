package com.github.andrdev.whatnext.ui;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimationBag;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimator;
import com.github.andrdev.whatnext.animation.SharedAnimationBag;
import com.github.andrdev.whatnext.animation.SharedTransitionAnimator;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.networking.HeardItBulkRequestListener;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.networking.RequestDistributor;
import com.github.andrdev.whatnext.networking.RetrofitGsonService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;


public class MovieActivity extends ActionBarActivity implements ActivityCallback {

    private final SpiceManager mSpiceManager = new SpiceManager(RetrofitGsonService.class);
    @Optional @InjectView(R.id.movingContainer)
    View mMovingContainer;
    @InjectView(R.id.poster)
     ImageView mPoster;
    @InjectView(R.id.title)
     TextView mTitle;
    @InjectView(R.id.progressBar)
     ProgressBar mProgressBar;
    @InjectView(R.id.toolbarHeardIt)
    Toolbar mToolbar;
    @InjectView(R.id.fragmentMain)
    FrameLayout mFrameLayout;
    MovingToolbarAnimator mMovingToolbarAnim;

    private boolean isFirstLaunch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);
        ButterKnife.inject(this);
        if (savedInstanceState == null) {
            isFirstLaunch = true;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.findFragmentById(R.id.fragmentMain) == null) {
            fragmentManager.beginTransaction().add(R.id.fragmentMain, new MovieFragment()).commit();
        }
        initViews();
        setToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);
        if (isFirstLaunch) {
            loadAnimation();
            isFirstLaunch = false;
        } else {
            TransitionDrawable drawable = (TransitionDrawable) (findViewById(R.id.layout)).getBackground();
            drawable.startTransition(0);
        }
    }

    @Override
    protected void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
        super.onStop();
    }

    void createMovingToolbarAnimator() {
        MovingToolbarAnimationBag bag =
                new MovingToolbarAnimationBag(mFrameLayout, mMovingContainer,
                        mMovingContainer.getHeight() - mToolbar.getHeight());
        mMovingToolbarAnim = new MovingToolbarAnimator(bag);
    }

    @Override
    public void sendRequest(int request, OnLoadListener listener, Bundle bundle) {
        mProgressBar.setVisibility(View.VISIBLE);
        String movie = bundle.getString(Constants.CURRENT_MOVIE);
        RetrofitSpiceRequest spiceRequest = RequestDistributor.getRequest(request, movie);
        mSpiceManager
                .execute(spiceRequest, request,
                        DurationInMillis.ALWAYS_EXPIRED, new HeardItBulkRequestListener<>(listener));
    }

    @Override
    public void loadComplete() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDataScroll(boolean isScrollDown) {
        if(mMovingContainer==null) {
            return;
        } else if(mMovingToolbarAnim == null) {
            createMovingToolbarAnimator();
        }
        mMovingToolbarAnim.scrollDetected(isScrollDown, this);
    }

    void initViews() {
        Movie movie = getIntent().getBundleExtra(Constants.BUNDLE).getParcelable(Constants.CURRENT_MOVIE);
        Picasso.with(this)
                .load("http:" + movie.getThumb_url()).into(mPoster);
        mTitle.setText(movie.getName());
    }

    void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
    }

    void loadAnimation() {
        SharedAnimationBag animationBag = getIntent().getBundleExtra(Constants.BUNDLE).getParcelable(Constants.ANIMATION);
        if (animationBag == null) return;
        Set<View> views = new HashSet<>();
        views.add(mPoster);
        views.add(mTitle);
        ViewGroup layout = (ViewGroup) findViewById(R.id.layout);
        new SharedTransitionAnimator().startAnimation(animationBag, views, layout);
    }
}
