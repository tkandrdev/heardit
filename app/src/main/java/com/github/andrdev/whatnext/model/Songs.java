package com.github.andrdev.whatnext.model;

import com.google.gson.annotations.Expose;

import java.util.List;


public class Songs implements HeardItBulkData<Song> {

    @Expose
    private List<Song> songs;

    @Override
    public List<Song> getList() {
        return songs;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Songs songs1 = (Songs) o;

        if (songs != null ? !songs.equals(songs1.songs) : songs1.songs != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return songs != null ? songs.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Songs{" +
                "songs=" + songs +
                '}';
    }
}
