package com.github.andrdev.whatnext.networking.NetworkRequests;

import com.github.andrdev.whatnext.model.Seasons;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by taiyokaze on 4/19/15.
 */
public class SeasonsRequest extends RetrofitSpiceRequest<Seasons, TuneFindService> {
    String mShowId;

    public SeasonsRequest(String id) {
        super(Seasons.class, TuneFindService.class);
        mShowId = id;
    }

    @Override
    public Seasons loadDataFromNetwork() throws Exception {
        return getService().seasons(mShowId);
    }
}
