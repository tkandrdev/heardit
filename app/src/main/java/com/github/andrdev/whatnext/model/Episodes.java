package com.github.andrdev.whatnext.model;

import com.google.gson.annotations.Expose;

import java.util.List;


public class Episodes implements HeardItBulkData<Episode> {

    @Expose
    private List<Episode> episodes;

    @Override
    public List<Episode> getList() {
        return episodes;
    }

    public List<Episode> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        this.episodes = episodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Episodes episodes1 = (Episodes) o;

        if (episodes != null ? !episodes.equals(episodes1.episodes) : episodes1.episodes != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return episodes != null ? episodes.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Episodes{" +
                "episodes=" + episodes +
                '}';
    }
}
