package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.ui.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MoviesRecyclerViewHolder extends BasicViewHolder<Movie> {
    @InjectView(R.id.poster)
     ImageView mPoster;
    @InjectView(R.id.title)
     TextView mTitle;
    @InjectView(R.id.circularProgressBar)
    ProgressBar progressBar;
    Callback callback = new Callback() {
        @Override
        public void onSuccess() {
            progressBar.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onError() {
            progressBar.setVisibility(View.INVISIBLE);
            mPoster.setImageResource(R.drawable.ic_launcher);
        }
    };
    public MoviesRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    @Override
    void bindView(Context context, Movie item) {
        progressBar.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load("http:" + item.getThumb_url())
                .transform(new RoundedTransformation(4, 0)).fit().into(mPoster, callback);
        mTitle.setText(item.getName());
    }
}
