package com.github.andrdev.whatnext.networking;

import android.os.Parcelable;

import com.github.andrdev.whatnext.model.HeardItData;

import java.util.List;


public interface OnLoadListener<E extends HeardItData & Parcelable> {
    void dataLoad(List<E> list);
}
