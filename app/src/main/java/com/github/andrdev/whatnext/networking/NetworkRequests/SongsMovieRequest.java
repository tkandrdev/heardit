package com.github.andrdev.whatnext.networking.NetworkRequests;

import com.github.andrdev.whatnext.model.Songs;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;


public class SongsMovieRequest extends RetrofitSpiceRequest<Songs, TuneFindService> {
    private String mMovieId;

    public SongsMovieRequest(String id) {
        super(Songs.class, TuneFindService.class);
        mMovieId = id;
    }

    @Override
    public Songs loadDataFromNetwork() throws Exception {
        return getService().movieSongs(mMovieId);
    }
}
