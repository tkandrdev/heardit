package com.github.andrdev.whatnext.networking;

import android.app.Application;

import com.github.andrdev.whatnext.database.HeardItRoboSpiceDBHelper;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Movies;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.model.Shows;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.ormlite.InDatabaseObjectPersisterFactory;

import java.util.ArrayList;
import java.util.List;


//spice service with ormlite cahcing
public class RetrofitOrmLiteService extends RetrofitBaseService {
    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        List<Class<?>> classCollection = new ArrayList<>();

        classCollection.add(Movies.class);
        classCollection.add(Shows.class);
        classCollection.add(Movie.class);
        classCollection.add(Show.class);

        HeardItRoboSpiceDBHelper databaseHelper =
                HeardItRoboSpiceDBHelper.createHeardItRoboSpiceDBHelper(application,
                        "heardit.db", 1);
        InDatabaseObjectPersisterFactory inDatabaseObjectPersisterFactory = new InDatabaseObjectPersisterFactory(
                application, databaseHelper, classCollection);
        cacheManager.addPersister(inDatabaseObjectPersisterFactory);
        return cacheManager;
    }
}
