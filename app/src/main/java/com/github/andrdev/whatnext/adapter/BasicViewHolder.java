package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.andrdev.whatnext.model.HeardItData;


abstract class BasicViewHolder<T extends HeardItData> extends RecyclerView.ViewHolder {

    public BasicViewHolder(View itemView) {
        super(itemView);
    }

    abstract void bindView(Context context, T item);
}
