package com.github.andrdev.whatnext.util;

import android.app.Activity;
import android.widget.EditText;


public class UiUtil {

    //checks if softkeyboard is visible
    public static boolean isSoftKeyboardVisible(Activity activity){
        if(activity.getCurrentFocus()!=null && activity.getCurrentFocus() instanceof EditText){
            return true;
        }
        return false;
    }
}
