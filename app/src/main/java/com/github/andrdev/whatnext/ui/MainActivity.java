package com.github.andrdev.whatnext.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.adapter.HeardItPagerAdapter;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.networking.HeardItBulkRequestListener;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.networking.RequestDistributor;
import com.github.andrdev.whatnext.networking.RetrofitOrmLiteService;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;


public class MainActivity extends BasePagerActivity {

    private SearchView searchView;

    @Override
    protected void onStart() {
        super.onStart();
        if(searchView != null){
            searchView.onActionViewCollapsed();
        }
    }

    @Override
    HeardItPagerAdapter getPagerAdapter() {
        return new HeardItPagerAdapter(getSupportFragmentManager(), "Main");
    }

    @Override
    Class<? extends RetrofitSpiceService> getSpiceService() {
        return RetrofitOrmLiteService.class;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_search) {
            onSearchRequested();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void sendRequest(int request, OnLoadListener listener, Bundle bundle) {
        RetrofitSpiceRequest spiceRequest;
        switch (request) {
            case DataTypesConst.MOVIES:
                spiceRequest = RequestDistributor.getRequest(request);
                break;
            case DataTypesConst.SHOWS:
                spiceRequest = RequestDistributor.getRequest(request);
                break;
            case DataTypesConst.SEASONS:
                Intent intentShow = new Intent(this, ShowActivity.class);
                intentShow.putExtra(Constants.BUNDLE, bundle);
                openActivity(intentShow);
                return;
            case DataTypesConst.SONGS:
                Intent intentSongs = new Intent(this, MovieActivity.class);
                intentSongs.putExtra(Constants.BUNDLE, bundle);
                openActivity(intentSongs);
                return;
            default:
                return;

        }
        mProgressBar.setVisibility(View.VISIBLE);
        getSpiceManager().execute(spiceRequest, request,
                DurationInMillis.ONE_DAY, new HeardItBulkRequestListener(listener));
    }

    private void openActivity(Intent intent) {
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
