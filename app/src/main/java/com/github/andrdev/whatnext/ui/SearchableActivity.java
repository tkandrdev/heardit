package com.github.andrdev.whatnext.ui;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.database.HeardItRoboSpiceDBHelper;
import com.github.andrdev.whatnext.database.HeardItSuggestionProvider;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Show;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;


public class SearchableActivity extends ActionBarActivity {

    private static final String TAG = SearchableActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        final String queryAction = intent.getAction();
        if (Intent.ACTION_SEARCH.equals(queryAction)) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            openSearchResultActivity(query);
        } else if (HeardItSuggestionProvider.INTENT_SHOW.equals(queryAction)) {
            openShow(intent);
        } else {
            openMovie(intent);
        }
    }

    private void openSearchResultActivity(String query) {
        try {
            ArrayList<Movie> movieList = new ArrayList<>
                    (HeardItRoboSpiceDBHelper.getHeardItRoboSpiceDBHelper().querySearchMovies(query));
            ArrayList<Show> showList = new ArrayList<>
                    (HeardItRoboSpiceDBHelper.getHeardItRoboSpiceDBHelper().querySearchShows(query));
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.MOVIE_LIST, movieList);
            bundle.putParcelableArrayList(Constants.SHOW_LIST, showList);
            Intent newIntent = new Intent(this, SearchResultActivity.class);
            newIntent.putExtra(Constants.BUNDLE, bundle);
            startActivity(newIntent);
        } catch (SQLException e) {
            Log.e(TAG, "Failed to lookup " + query, e);
        }
    }

    private void openShow(Intent intent) {
        String key = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
        try {
            Show show = HeardItRoboSpiceDBHelper.getHeardItRoboSpiceDBHelper().queryShow(key);
            Picasso.with(this)
                    .load("http:" + show.getThumb_url()).stableKey(show.getName()).fetch();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.CURRENT_SHOW, show);
            Intent newIntent = new Intent(this, ShowActivity.class);
            newIntent.putExtra(Constants.BUNDLE, bundle);
            startActivity(newIntent);
        } catch (SQLException e) {
            Log.e(TAG, "Failed to lookup " + key, e);
        }
    }

    private void openMovie(Intent intent) {
        String key = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
        try {
            Movie movie = HeardItRoboSpiceDBHelper.getHeardItRoboSpiceDBHelper().queryMovie(key);
            Picasso.with(this)
                    .load("http:" + movie.getThumb_url()).stableKey(movie.getName()).fetch();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.CURRENT_MOVIE, movie);
            Intent newIntent = new Intent(this, MovieActivity.class);
            newIntent.putExtra(Constants.BUNDLE, bundle);
            startActivity(newIntent);
        } catch (SQLException e) {
            Log.e(TAG, "Failed to lookup " + key, e);
        }

    }
}