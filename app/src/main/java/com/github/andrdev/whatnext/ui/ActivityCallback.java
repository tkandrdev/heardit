package com.github.andrdev.whatnext.ui;

import android.os.Bundle;

import com.github.andrdev.whatnext.networking.OnLoadListener;


public interface ActivityCallback {

    void sendRequest(int request, OnLoadListener listener, Bundle bundle);

    void loadComplete();

    void onDataScroll(boolean isScrollDown);
}
