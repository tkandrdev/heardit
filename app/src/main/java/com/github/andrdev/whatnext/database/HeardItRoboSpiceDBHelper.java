package com.github.andrdev.whatnext.database;

import android.content.Context;

import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Show;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.octo.android.robospice.persistence.ormlite.RoboSpiceDatabaseHelper;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;


public class HeardItRoboSpiceDBHelper extends RoboSpiceDatabaseHelper {

    private static HeardItRoboSpiceDBHelper heardItRoboSpiceDBHelper;

    private HeardItRoboSpiceDBHelper(Context context, String databaseName, int databaseVersion) {
        super(context, databaseName, databaseVersion);
    }

    public static HeardItRoboSpiceDBHelper createHeardItRoboSpiceDBHelper
            (Context context, String databaseName, int databaseVersion) {
        if (heardItRoboSpiceDBHelper == null) {
            heardItRoboSpiceDBHelper =
                    new HeardItRoboSpiceDBHelper(context, databaseName, databaseVersion);
        }
        return heardItRoboSpiceDBHelper;
    }

    public static HeardItRoboSpiceDBHelper getHeardItRoboSpiceDBHelper() {
        return heardItRoboSpiceDBHelper != null ? heardItRoboSpiceDBHelper : null;
    }

    //searches for query in name column of the show table
    public List<Show> querySearchShows(String query) throws SQLException {
        RuntimeExceptionDao<Show, Integer> reDao =
                heardItRoboSpiceDBHelper.getRuntimeExceptionDao(Show.class);
        List<Show> showList = reDao.query(reDao
                .queryBuilder()
                .where().like("name", "%" + query + "%").prepare());
        return showList;
    }

    //searches for query in name field of the movie table
    public List<Movie> querySearchMovies(String query) throws SQLException {
        RuntimeExceptionDao<Movie, Integer> reDao =
                heardItRoboSpiceDBHelper.getRuntimeExceptionDao(Movie.class);
        List<Movie> movieList = reDao.query(reDao
                .queryBuilder()
                .where().like("name", "%" + query + "%").prepare());
        return movieList;
    }

    //gets show object with name field eq query
    public Show queryShow(String query) throws SQLException {
        RuntimeExceptionDao<Show, Integer> reDao =
                heardItRoboSpiceDBHelper.getRuntimeExceptionDao(Show.class);
        Show show = reDao.queryForEq("name", query).get(0);
        return show;
    }

    //gets movie object with name field eq query
    public Movie queryMovie(String query) throws SQLException {
        RuntimeExceptionDao<Movie, Integer> reDao =
                heardItRoboSpiceDBHelper.getRuntimeExceptionDao(Movie.class);
        Movie movie = reDao.queryForEq("name", query).get(0);
        return movie;
    }

    //deletes data from db.
    @Override
    public <T> void deleteFromDataBase(Collection<T> objectList, Class<T> modelObjectClass) throws SQLException {
        super.deleteFromDataBase(objectList, modelObjectClass);
        super.clearTableFromDataBase(modelObjectClass);
    }
}
