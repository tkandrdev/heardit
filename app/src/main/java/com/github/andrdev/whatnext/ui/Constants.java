package com.github.andrdev.whatnext.ui;


public class Constants {
    public final static String ANIMATION = "Animation";
    public final static String BUNDLE = "Bundle";
    public final static String CURRENT_SHOW = "CurrentShow";
    public final static String CURRENT_SEASON = "CurrentSeason";
    public final static String CURRENT_SEASON_NUMBER = "CurrentSeasonNumber";
    public final static String CURRENT_EPISODE = "CurrentEpisode";
    public final static String EPISODE_POSITION = "EpisodePosition";
    public final static String SEASON_POSITION = "SeasonPosition";
    public final static String MOVIE_LIST = "MovieList";
    public final static String SHOW_LIST = "ShowList";
    public final static String SEASON_LIST = "SeasonList";
    public final static String EPISODE_LIST = "EpisodeList";
    public final static String CURRENT_MOVIE = "CurrentMovie";
}
