package com.github.andrdev.whatnext.model;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@DatabaseTable(tableName = "movie_request")
public class Movies implements HeardItBulkData<Movie> {

    @DatabaseField(generatedId = true)
    private int id;

    @Expose
    @ForeignCollectionField(eager = true)
    private Collection<Movie> movies;

    @Override
    public List<Movie> getList() {
        return new ArrayList<>(movies);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Collection<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movies movies1 = (Movies) o;

        if (id != movies1.id) return false;
        if (movies != null ? !movies.equals(movies1.movies) : movies1.movies != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (movies != null ? movies.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Movies{" +
                "id=" + id +
                ", movies=" + movies +
                '}';
    }
}
