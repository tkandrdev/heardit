package com.github.andrdev.whatnext.model;

import java.util.List;


public interface HeardItBulkData<T> {
    public List<T> getList();
}
