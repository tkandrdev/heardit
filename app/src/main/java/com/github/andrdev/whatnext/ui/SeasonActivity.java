package com.github.andrdev.whatnext.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.adapter.FragmentTabHelper;
import com.github.andrdev.whatnext.adapter.HeardItPagerAdapter;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Season;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.networking.HeardItBulkRequestListener;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.networking.RequestDistributor;
import com.github.andrdev.whatnext.networking.RetrofitGsonService;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;


public class SeasonActivity extends BasePagerActivity {

    private Show mShow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShow = getBundle().getParcelable(Constants.CURRENT_SHOW);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState == null) {
            int mSeasonPosition = getBundle().getInt(Constants.SEASON_POSITION);
            setViewPagerPosition(mSeasonPosition);
        }
    }

    @Override
    HeardItPagerAdapter getPagerAdapter() {
        return new HeardItPagerAdapter(getSupportFragmentManager(), new FragmentTabHelper<>(getBundle()));
    }

    @Override
    Class<? extends RetrofitSpiceService> getSpiceService() {
        return RetrofitGsonService.class;
    }

    @Override
    public void sendRequest(int requestType, OnLoadListener listener, Bundle bundle) {
        RetrofitSpiceRequest spiceRequest;

        Season mSeason = bundle.getParcelable(Constants.CURRENT_SEASON);

        StringBuilder requestId = new StringBuilder();
        requestId.append(mShow.getName()).append(mSeason.getNumber());
        switch (requestType) {
            case DataTypesConst.EPISODES:
                mProgressBar.setVisibility(View.VISIBLE);
                spiceRequest = RequestDistributor.getRequest(requestType,
                        mShow.getId(), mSeason.getNumber());
                getSpiceManager().execute(spiceRequest, requestId,
                        DurationInMillis.ALWAYS_EXPIRED, new HeardItBulkRequestListener(listener));
                break;
            case DataTypesConst.SONGS:
                openActivity(bundle);
                break;
            default:
                break;
        }
    }

    void openActivity(Bundle bundle) {
        Intent intent = new Intent(this, EpisodeActivity.class);
        intent.putExtra(Constants.BUNDLE, bundle);
        startActivity(intent);
    }
}
