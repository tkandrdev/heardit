package com.github.andrdev.whatnext.adapter;

import android.support.v4.app.Fragment;


abstract class TabHelper {

    abstract int getCount();

    abstract Fragment getFragment(int position);

    abstract String getTitle(int position);
}
