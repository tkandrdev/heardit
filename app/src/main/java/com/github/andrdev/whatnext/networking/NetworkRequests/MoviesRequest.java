package com.github.andrdev.whatnext.networking.NetworkRequests;

import com.github.andrdev.whatnext.model.Movies;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;


public class MoviesRequest extends RetrofitSpiceRequest<Movies, TuneFindService> {

    public MoviesRequest() {
        super(Movies.class, TuneFindService.class);
    }

    @Override
    public Movies loadDataFromNetwork() throws Exception {
        return getService().movies();
    }
}
