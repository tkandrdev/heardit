package com.github.andrdev.whatnext.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.adapter.HeardItPagerAdapter;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimationBag;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimator;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.view.SlidingTabLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;


public class SearchResultActivity extends ActionBarActivity
        implements ActivityCallback {

    @Optional @InjectView(R.id.movingContainer)
    View mMovingContainer;
    @InjectView(R.id.toolbarHeardIt)
    Toolbar mToolbar;
    @InjectView(R.id.viewpager)
    ViewPager mViewPager;
    @InjectView(R.id.viewpager_tabs)
    SlidingTabLayout mSlidingTabStrip;
    MovingToolbarAnimator mMovingToolbarAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        ButterKnife.inject(this);
        setToolbar();
        initPager();
    }

    void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    void initPager() {
        HeardItPagerAdapter mPagerAdapter = new HeardItPagerAdapter(getSupportFragmentManager(), "Main");
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.space_normal));
        mSlidingTabStrip.setViewPager(mViewPager);
    }

    @Override
    public void loadComplete() {
    }

    void createMovingToolbarAnimation() {
        MovingToolbarAnimationBag bag =
                new MovingToolbarAnimationBag(mViewPager, mMovingContainer, mToolbar.getHeight());
        mMovingToolbarAnim = new MovingToolbarAnimator(bag);
    }

    @Override
    public void onDataScroll(boolean isScrollDown) {
        if(mMovingContainer==null) {
            return;
        } else if(mMovingToolbarAnim == null) {
            createMovingToolbarAnimation();
        }
        mMovingToolbarAnim.scrollDetected(isScrollDown, this);

    }

    @Override
    public void sendRequest(int request, OnLoadListener listener, Bundle bundle) {
        switch (request) {
            case DataTypesConst.SEASONS:
                Intent showIntent = new Intent(this, ShowActivity.class);
                showIntent.putExtra(Constants.BUNDLE, bundle);
                openActivity(showIntent);
                break;
            case DataTypesConst.SONGS:
                Intent movieIntent = new Intent(this, MovieActivity.class);
                movieIntent.putExtra(Constants.BUNDLE, bundle);
                openActivity(movieIntent);
                break;
            default:
                break;

        }
    }

    private void openActivity(Intent intent) {
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
