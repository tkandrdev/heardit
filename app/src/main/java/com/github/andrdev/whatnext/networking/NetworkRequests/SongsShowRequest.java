package com.github.andrdev.whatnext.networking.NetworkRequests;

import com.github.andrdev.whatnext.model.Songs;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by taiyokaze on 4/19/15.
 */
public class SongsShowRequest extends RetrofitSpiceRequest<Songs, TuneFindService> {
    String mShowId;
    String mSeasonId;
    String mEpisodeId;

    public SongsShowRequest(String showId, String seasonNumber, String episodeId) {
        super(Songs.class, TuneFindService.class);
        mShowId = showId;
        mSeasonId = seasonNumber;
        mEpisodeId = episodeId;
    }

    @Override
    public Songs loadDataFromNetwork() throws Exception {
        return getService().showSongs(mShowId, mSeasonId, mEpisodeId);
    }
}