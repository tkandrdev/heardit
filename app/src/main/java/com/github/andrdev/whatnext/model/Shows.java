package com.github.andrdev.whatnext.model;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@DatabaseTable(tableName = "show_request")
public class Shows implements HeardItBulkData<Show> {

    @DatabaseField(generatedId = true)
    private int id;

    @Expose
    @ForeignCollectionField(eager = true)
    private Collection<Show> shows;

    @Override
    public List<Show> getList() {
        return new ArrayList<>(shows);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Show> getShows() {
        return shows;
    }

    public void setShows(Collection<Show> shows) {
        this.shows = shows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shows shows1 = (Shows) o;

        if (id != shows1.id) return false;
        if (shows != null ? !shows.equals(shows1.shows) : shows1.shows != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (shows != null ? shows.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Shows{" +
                "id=" + id +
                ", shows=" + shows +
                '}';
    }
}
