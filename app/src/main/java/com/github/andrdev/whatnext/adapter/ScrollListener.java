package com.github.andrdev.whatnext.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.github.andrdev.whatnext.ui.ActivityCallback;


public class ScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager mLayoutManager;
    int mLastFirstVisibleItem = 0;
    int mShift = 0;
    ActivityCallback mActivityCallback;

    public ScrollListener(LinearLayoutManager lm, int shift, ActivityCallback ac) {
        mLayoutManager = lm;
        mShift = shift;
        mActivityCallback = ac;
    }

    //calculates scroll direction
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        final int currentFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

        if (currentFirstVisibleItem > this.mLastFirstVisibleItem + mShift) {
            mActivityCallback.onDataScroll(true);

        } else if (currentFirstVisibleItem + mShift < this.mLastFirstVisibleItem) {
            mActivityCallback.onDataScroll(false);
        } else return;

        this.mLastFirstVisibleItem = currentFirstVisibleItem;
    }

}
