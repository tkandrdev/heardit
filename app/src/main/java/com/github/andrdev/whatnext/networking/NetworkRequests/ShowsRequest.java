package com.github.andrdev.whatnext.networking.NetworkRequests;


import com.github.andrdev.whatnext.model.Shows;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class ShowsRequest extends RetrofitSpiceRequest<Shows, TuneFindService> {

    public ShowsRequest() {
        super(Shows.class, TuneFindService.class);
    }

    @Override
    public Shows loadDataFromNetwork() throws Exception {
        return getService().shows();
    }
}
