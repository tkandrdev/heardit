package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.adapter.FragmentTabHelper;
import com.github.andrdev.whatnext.adapter.HeardItPagerAdapter;
import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.Season;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.networking.HeardItBulkRequestListener;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.networking.RequestDistributor;
import com.github.andrdev.whatnext.networking.RetrofitGsonService;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;


public class EpisodeActivity extends BasePagerActivity {

    private Show mShow;
    private Season mSeason;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mShow = getBundle().getParcelable(Constants.CURRENT_SHOW);
        mSeason = getBundle().getParcelable(Constants.CURRENT_SEASON);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //if first launch, than set pager position
        if (savedInstanceState == null) {
            int mCurrentEpisodePosition = getBundle().getInt(Constants.EPISODE_POSITION);
            setViewPagerPosition(mCurrentEpisodePosition);
        }
    }

    @Override
    HeardItPagerAdapter getPagerAdapter() {
        return new HeardItPagerAdapter(getSupportFragmentManager(), new FragmentTabHelper<>(getBundle()));
    }

    @Override
    Class<? extends RetrofitSpiceService> getSpiceService() {
        return RetrofitGsonService.class;
    }

    @Override
    public void sendRequest(int requestType, OnLoadListener listener, Bundle bundle) {
        mProgressBar.setVisibility(View.VISIBLE);

        Episode mEpisode = bundle.getParcelable(Constants.CURRENT_EPISODE);

        if (mEpisode == null) return;

        String cacheKey = mShow.getId() + mSeason.getNumber() + mEpisode.getId();
        RetrofitSpiceRequest spiceRequest = RequestDistributor.getRequest(requestType,
                mShow.getId(), mSeason.getNumber(), mEpisode.getId());

        getSpiceManager().execute(spiceRequest, cacheKey,
                DurationInMillis.ALWAYS_EXPIRED, new HeardItBulkRequestListener<>(listener));
    }

}
