package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Episode;

import java.util.ArrayList;


public class SeasonFragment extends BaseListFragment<Episode> {


    @Override
    int getDataType() {
        return DataTypesConst.EPISODES;
    }

    @Override
    int getLayout() {
        return R.layout.fragment_rv;
    }

    @Override
    void onRowClick(View view, int position) {
        Bundle bundle = getBundle();
        bundle.putParcelableArrayList(Constants.EPISODE_LIST, new ArrayList<>(getDataList()));
        bundle.putParcelable(Constants.CURRENT_EPISODE, getDataList().get(position));
        bundle.putInt(Constants.EPISODE_POSITION, position);
        getActivityCallback().sendRequest(DataTypesConst.SONGS, this, bundle);
    }
}