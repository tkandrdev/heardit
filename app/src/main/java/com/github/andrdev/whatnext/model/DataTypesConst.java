package com.github.andrdev.whatnext.model;

/**
 * Created by taiyokaze on 4/10/15.
 */
public class DataTypesConst {

    public final static int SHOWS = 1;

    public final static int MOVIES = 2;

    public final static int SEASONS = 3;

    public final static int EPISODES = 4;

    public final static int SONGS = 5;
}
