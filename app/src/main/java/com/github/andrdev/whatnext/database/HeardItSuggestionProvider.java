package com.github.andrdev.whatnext.database;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.ui.Constants;

import java.sql.SQLException;
import java.util.List;


//basic SearchRecentSuggestionsProvider realization.
public class HeardItSuggestionProvider extends SearchRecentSuggestionsProvider {

    private static final String TAG = HeardItSuggestionProvider.class.getSimpleName();
    private static final String AUTHORITY = HeardItSuggestionProvider.class
            .getName();
    public static final String INTENT_SHOW = "com.github.andrdev.whatnext.VIEW_SHOW";
    public static final String INTENT_MOVIE = "com.github.andrdev.whatnext.VIEW_MOVIE";
    private static final int MODE = DATABASE_MODE_QUERIES;
    private static final String[] COLUMNS = {
            "_id",
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_INTENT_ACTION,
            SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA};

    public HeardItSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        String query = selectionArgs[0].toLowerCase();
        if (query.length() == 0) {
            return null;
        }

        MatrixCursor cursor = new MatrixCursor(COLUMNS);
        HeardItRoboSpiceDBHelper dbHelper = HeardItRoboSpiceDBHelper.getHeardItRoboSpiceDBHelper();
        try {
            List<Movie> listMovies = dbHelper.querySearchMovies(query);
            List<Show> listShows = dbHelper.querySearchShows(query);
            int n = 0;
            for (Movie movie : listMovies) {
                cursor.addRow(createRow(n, movie.getName(), INTENT_MOVIE, movie.getName()));
                n++;
            }
            for (Show show : listShows) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CURRENT_SHOW, show);
                cursor.addRow(createRow(n, show.getName(), INTENT_SHOW, show.getName()));
                n++;
            }
        } catch (SQLException e) {
            Log.e(TAG, "Failed to lookup " + query, e);
        }
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    private Object[] createRow(Integer id, String name, String intent, String key) {
        return new Object[]{id, name, intent, key};
    }

}