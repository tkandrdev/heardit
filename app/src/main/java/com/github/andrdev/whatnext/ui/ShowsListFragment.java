package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.animation.SharedAnimationBag;
import com.github.andrdev.whatnext.animation.SharedTransitionAnimator;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Show;

import java.util.List;


public class ShowsListFragment extends BaseListFragment<Show> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle bundle = getActivity().getIntent().getBundleExtra(Constants.BUNDLE);
        if (bundle != null) {
            List<Show> showList = bundle.getParcelableArrayList(Constants.SHOW_LIST);
            setData(showList);
        }
        showDivider(false);
    }

    @Override
    int getDataType() {
        return DataTypesConst.SHOWS;
    }

    @Override
    int getLayout() {
        return R.layout.fragment_shows;
    }

    @Override
    void onRowClick(View view, int position) {
        SharedAnimationBag animationBag = setDataForAnimation(view);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.ANIMATION, animationBag);
        bundle.putParcelable(Constants.CURRENT_SHOW, getDataList().get(position));
        getActivityCallback().sendRequest(DataTypesConst.SEASONS, null, bundle);
    }

    private SharedAnimationBag setDataForAnimation(View view) {
        View[] views = {view.findViewById(R.id.poster),
                view.findViewById(R.id.title)};
        SharedAnimationBag animationBag = new SharedAnimationBag(SharedTransitionAnimator.SHARED_ELEMENTS, views);
        return animationBag;
    }
}
