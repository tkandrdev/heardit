package com.github.andrdev.whatnext.animation;

import android.view.View;


//view parameters for shared animation
public class SharedViewAnimateParam {

    private float leftDelta;
    private float topDelta;
    private float scaleX;
    private float scaleY;
    private long duration = 1000;
    private View view;

    public float getLeftDelta() {
        return leftDelta;
    }

    public void setLeftDelta(float leftDelta) {
        this.leftDelta = leftDelta;
    }

    public float getTopDelta() {
        return topDelta;
    }

    public void setTopDelta(float topDelta) {
        this.topDelta = topDelta;
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}
