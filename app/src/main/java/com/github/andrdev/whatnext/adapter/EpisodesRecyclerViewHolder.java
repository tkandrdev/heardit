package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.Episode;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class EpisodesRecyclerViewHolder extends BasicViewHolder<Episode> {

    @InjectView(R.id.number)
     TextView mNumber;
    @InjectView(R.id.title)
     TextView mTitle;

    public EpisodesRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    @Override
    void bindView(Context context, Episode item) {
        mNumber.setText(item.getNumber());
        mTitle.setText(item.getName());
    }
}