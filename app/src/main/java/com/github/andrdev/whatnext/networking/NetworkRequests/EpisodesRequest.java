package com.github.andrdev.whatnext.networking.NetworkRequests;

import com.github.andrdev.whatnext.model.Episodes;
import com.github.andrdev.whatnext.networking.TuneFindService;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by taiyokaze on 4/19/15.
 */
public class EpisodesRequest extends RetrofitSpiceRequest<Episodes, TuneFindService> {
    String mShowId;
    String mSeasonId;

    public EpisodesRequest(String showId, String seasonNumber) {
        super(Episodes.class, TuneFindService.class);
        mShowId = showId;
        mSeasonId = seasonNumber;
    }

    @Override
    public Episodes loadDataFromNetwork() throws Exception {
        return getService().episodes(mShowId, mSeasonId);
    }
}