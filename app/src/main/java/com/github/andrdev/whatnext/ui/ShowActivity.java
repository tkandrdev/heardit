package com.github.andrdev.whatnext.ui;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimationBag;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimator;
import com.github.andrdev.whatnext.animation.SharedAnimationBag;
import com.github.andrdev.whatnext.animation.SharedTransitionAnimator;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.networking.RetrofitGsonService;
import com.github.andrdev.whatnext.networking.HeardItBulkRequestListener;
import com.github.andrdev.whatnext.networking.OnLoadListener;
import com.github.andrdev.whatnext.networking.RequestDistributor;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;


public class ShowActivity extends ActionBarActivity implements ActivityCallback {

    private final SpiceManager mSpiceManager = new SpiceManager(RetrofitGsonService.class);
    private OnLoadListener mShowListener, mSeasonListener, mEpisodeListener;
    private FragmentManager mFragmentManager;
    private Show mShow;
    @Optional @InjectView(R.id.movingContainer)
    View mMovingContainer;
    @InjectView(R.id.poster)
    ImageView mPoster;
    @InjectView(R.id.title)
    TextView mTitle;
    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;
    @InjectView(R.id.toolbarHeardIt)
    Toolbar mToolbar;
    @InjectView(R.id.fragmentOne)
    FrameLayout mFrameLayoutOne;

    private boolean isFirstLaunch = false;
    String mSeasonNumber = "1";
    MovingToolbarAnimator mMovingToolbarAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);
        ButterKnife.inject(this);
        mFragmentManager = getSupportFragmentManager();
        setToolbar();
        if (savedInstanceState == null) {
            isFirstLaunch = true;
        }
        if (mFragmentManager.findFragmentById(R.id.fragmentOne) == null) {
            loadFragments();
        }

        mShow = getIntent().getBundleExtra(Constants.BUNDLE).getParcelable(Constants.CURRENT_SHOW);

        getSupportActionBar().setTitle("");
        mTitle.setText(mShow.getName());
        Picasso.with(this)
                .load("http:" + mShow.getThumb_url()).into(mPoster);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);
        if (isFirstLaunch) {
            loadAnimation();
            isFirstLaunch = false;
        } else {
        TransitionDrawable drawable = (TransitionDrawable) (findViewById(R.id.layout)).getBackground();
        drawable.startTransition(0);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSeasonNumber = savedInstanceState.getString(Constants.CURRENT_SEASON_NUMBER);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.CURRENT_SEASON_NUMBER, mSeasonNumber);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
        super.onStop();
    }

    void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    void loadFragments() {
        mFragmentManager.beginTransaction().add(R.id.fragmentOne, new ShowFragment()).commit();

        if (findViewById(R.id.fragmentTwo) != null) {
            mFragmentManager.beginTransaction().add(R.id.fragmentTwo, new SeasonFragment()).commit();
            mFragmentManager.beginTransaction().add(R.id.fragmentThree, new EpisodeFragment()).commit();
        }
    }

    void createMovingToolbarAnimator() {
        MovingToolbarAnimationBag bag =
                new MovingToolbarAnimationBag(mFrameLayoutOne, mMovingContainer,
                        mMovingContainer.getHeight() - mToolbar.getHeight());
        mMovingToolbarAnim = new MovingToolbarAnimator(bag);
    }

    @Override
    public void sendRequest(int requestType, OnLoadListener listener, Bundle bundle) {
        mShowListener = (OnLoadListener) mFragmentManager.findFragmentById(R.id.fragmentOne);
        mSeasonListener = (OnLoadListener) mFragmentManager.findFragmentById(R.id.fragmentTwo);
        mEpisodeListener = (OnLoadListener) mFragmentManager.findFragmentById(R.id.fragmentThree);

        RetrofitSpiceRequest spiceRequest;
        OnLoadListener onLoadListener;
        StringBuilder requestId = new StringBuilder();
        mSeasonNumber = bundle.getString(Constants.CURRENT_SEASON_NUMBER, mSeasonNumber);
        requestId.append(mShow.getName());
        switch (requestType) {
            case DataTypesConst.SEASONS:
                onLoadListener = mShowListener;
                spiceRequest = RequestDistributor.getRequest(requestType, mShow.getId());
                break;
            case DataTypesConst.EPISODES:
                if (mSeasonListener == null) {
                    openSeasonActivity(bundle);
                    return;
                }
                onLoadListener = mSeasonListener;
                requestId.append(mSeasonNumber);
                spiceRequest = RequestDistributor.getRequest(requestType, mShow.getId(), mSeasonNumber);
                break;
            case DataTypesConst.SONGS:
                Episode mEpisode = bundle.getParcelable(Constants.CURRENT_EPISODE);
                String currentEpisode = mEpisode.getId();
                requestId.append(mSeasonNumber).append(currentEpisode);
                spiceRequest = RequestDistributor.getRequest(requestType,
                                mShow.getId(), mSeasonNumber, currentEpisode);
                onLoadListener = mEpisodeListener;
                break;
            default:
                return;
        }
        if (onLoadListener == null) {
            onLoadListener = listener;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        mSpiceManager.execute(spiceRequest, requestId,
                DurationInMillis.ALWAYS_EXPIRED, new HeardItBulkRequestListener(onLoadListener));
    }

    @Override
    public void loadComplete() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onDataScroll(boolean isScrollDown) {
        if(mMovingContainer==null) {
            return;
        } else if(mMovingToolbarAnim == null) {
            createMovingToolbarAnimator();
        }
        mMovingToolbarAnim.scrollDetected(isScrollDown, this);
    }

    void openSeasonActivity(Bundle bundle) {
        bundle.putParcelable(Constants.CURRENT_SHOW, mShow);
        Intent intent = new Intent(this, SeasonActivity.class);
        intent.putExtra(Constants.BUNDLE, bundle);
        startActivity(intent);
    }

    void loadAnimation() {
        SharedAnimationBag animationBag = getIntent().getBundleExtra(Constants.BUNDLE)
                .getParcelable(Constants.ANIMATION);
        if (animationBag == null) return;
        Set<View> views = new HashSet<>();
        views.add(mPoster);
        views.add(mTitle);
        ViewGroup layout = (ViewGroup) findViewById(R.id.layout);
        new SharedTransitionAnimator().startAnimation(animationBag, views, layout);
    }
}