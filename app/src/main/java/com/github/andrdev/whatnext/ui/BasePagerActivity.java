package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.adapter.HeardItPagerAdapter;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimationBag;
import com.github.andrdev.whatnext.animation.MovingToolbarAnimator;
import com.github.andrdev.whatnext.view.SlidingTabLayout;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;


public abstract class BasePagerActivity extends ActionBarActivity
        implements ActivityCallback {

    private final SpiceManager mSpiceManager = new SpiceManager(getSpiceService());
    @Optional @InjectView(R.id.movingContainer)
    View mMovingContainer;
    @InjectView(R.id.viewpager)
    ViewPager mViewPager;
    private Bundle bundle;
    @InjectView(R.id.viewpager_tabs)
    SlidingTabLayout mSlidingTabStrip;
    @InjectView(R.id.progressBar)
     ProgressBar mProgressBar;
    @InjectView(R.id.toolbarHeardIt)
    Toolbar mToolbar;
    MovingToolbarAnimator mMovingToolbarAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        ButterKnife.inject(this);
        bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        setToolbar();
        initPager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSpiceManager.start(this);

    }

    @Override
    protected void onStop() {
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
        super.onStop();
    }

    Bundle getBundle() {
        return bundle;
    }

    void setToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    Toolbar getToolbar() {
        return mToolbar;
    }

    void initPager() {
        HeardItPagerAdapter mPagerAdapter = getPagerAdapter();
        mViewPager.setAdapter(mPagerAdapter);
        mSlidingTabStrip.setViewPager(mViewPager);
    }

    void createMovingToolbarAnimator() {
        MovingToolbarAnimationBag bag =
                new MovingToolbarAnimationBag(mViewPager, mMovingContainer, mToolbar.getHeight());
        mMovingToolbarAnim = new MovingToolbarAnimator(bag);
    }

    //
    abstract HeardItPagerAdapter getPagerAdapter();

    SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    void setViewPagerPosition(int position) {
        mViewPager.setCurrentItem(position);
    }

    //if this is the last request, than we hide progressbar
    @Override
    public void loadComplete() {
        if(mSpiceManager.getPendingRequestCount()>=1){
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    abstract Class<? extends RetrofitSpiceService> getSpiceService();

    @Override
    public void onDataScroll(boolean isScrollDown) {
        if(mMovingContainer==null) {
            return;
        } else if(mMovingToolbarAnim == null) {
            createMovingToolbarAnimator();
        }
        mMovingToolbarAnim.scrollDetected(isScrollDown, this);
    }
}
