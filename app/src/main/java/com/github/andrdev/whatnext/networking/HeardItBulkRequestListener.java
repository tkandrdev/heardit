package com.github.andrdev.whatnext.networking;

import android.support.v4.app.Fragment;
import android.transitions.everywhere.Transition;
import android.util.Log;

import com.github.andrdev.whatnext.model.HeardItBulkData;
import com.github.andrdev.whatnext.ui.ActivityCallback;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.PendingRequestListener;

import java.lang.ref.WeakReference;


public class HeardItBulkRequestListener<BD extends HeardItBulkData> implements PendingRequestListener<BD> {

    private final WeakReference<OnLoadListener> mListener;

    public HeardItBulkRequestListener(OnLoadListener listener) {
        this.mListener = new WeakReference<>(listener);
    }

    @Override
    public void onRequestNotFound() {
        ActivityCallback callback = (ActivityCallback) ((Fragment) mListener.get()).getActivity();
        callback.loadComplete();
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        ActivityCallback callback = (ActivityCallback) ((Fragment) mListener.get()).getActivity();
        callback.loadComplete();
    }

    @Override
    public void onRequestSuccess(BD result) {
        OnLoadListener listener = mListener.get();
        if (listener==null) {
            return;
        }
            ActivityCallback activity = (ActivityCallback) ((Fragment) listener).getActivity();
            activity.loadComplete();
            listener.dataLoad(result.getList());
    }
}
