package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.animation.SharedAnimationBag;
import com.github.andrdev.whatnext.animation.SharedTransitionAnimator;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Movie;

import java.util.List;


public class MoviesListFragment extends BaseListFragment<Movie> {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getActivity().getIntent().getBundleExtra(Constants.BUNDLE);
        if (bundle != null && bundle.getParcelableArrayList(Constants.MOVIE_LIST) !=null) {
            List<Movie> movieList = bundle.getParcelableArrayList(Constants.MOVIE_LIST);
            setData(movieList);
        }
        showDivider(false);
    }

    @Override
    int getLayout() {
        return R.layout.fragment_movies;
    }

    @Override
    int getDataType() {
        return DataTypesConst.MOVIES;
    }

    @Override
    void onRowClick(View view, int position) {
        SharedAnimationBag animationBag = setDataForAnimation(view);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.ANIMATION, animationBag);
        bundle.putParcelable(Constants.CURRENT_MOVIE, getDataList().get(position));
        getActivityCallback().sendRequest(DataTypesConst.SONGS, null, bundle);
    }

    private SharedAnimationBag setDataForAnimation(View view) {
        View[] views = {view.findViewById(R.id.poster),
                view.findViewById(R.id.title)};
        SharedAnimationBag animationBag = new SharedAnimationBag(SharedTransitionAnimator.SHARED_ELEMENTS, views);
        return animationBag;
    }
}
