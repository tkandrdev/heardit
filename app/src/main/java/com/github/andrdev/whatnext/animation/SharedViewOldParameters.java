package com.github.andrdev.whatnext.animation;

import android.os.Parcel;
import android.os.Parcelable;


// stores view parameters from an old screen
public class SharedViewOldParameters implements Parcelable {

    private String tag;
    private int width;
    private int height;
    private int top;
    private int left;

    public SharedViewOldParameters() {

    }

    public String getView() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tag);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeInt(top);
        dest.writeInt(left);
    }

    public static final Parcelable.Creator<SharedViewOldParameters> CREATOR = new Parcelable.Creator<SharedViewOldParameters>() {
        public SharedViewOldParameters createFromParcel(Parcel in) {
            return new SharedViewOldParameters(in);
        }

        public SharedViewOldParameters[] newArray(int size) {
            return new SharedViewOldParameters[size];
        }
    };

    SharedViewOldParameters(Parcel in) {
        tag = in.readString();
        width = in.readInt();
        height = in.readInt();
        top = in.readInt();
        left = in.readInt();
    }
}
