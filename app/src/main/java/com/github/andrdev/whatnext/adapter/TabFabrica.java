package com.github.andrdev.whatnext.adapter;


public class TabFabrica {

    static TabHelper giveHelper(String helper) {
        if (helper == null || helper.isEmpty()) {
            return null;
        }
        switch (helper) {
            case "Main":
                return new MainTabHelper();
            default:
                return null;
        }
    }
}
