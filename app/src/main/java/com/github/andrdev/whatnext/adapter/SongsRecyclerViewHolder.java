package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.Song;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SongsRecyclerViewHolder extends BasicViewHolder<Song> {

    @InjectView(R.id.artist)
     TextView mArtist;
    @InjectView(R.id.title)
     TextView mTitle;
    @InjectView(R.id.scene)
     TextView mScene;

    public SongsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    @Override
    void bindView(Context context, Song item) {
        mArtist.setText(item.getArtist().getName());
        mTitle.setText(item.getName());
        mScene.setText(item.getScene());
    }
}