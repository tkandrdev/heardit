package com.github.andrdev.whatnext.animation;

import android.graphics.drawable.TransitionDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.github.andrdev.whatnext.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


//translates shared views to their position, and scales them to their actual size.
//not shared views animated from alpha 0 to 1.
// Main layout must have a TransitionDrawable background.

public class SharedTransitionAnimator {

    //animation id. Planing to add a few more
    public final static int SHARED_ELEMENTS = 1;

    //checks animation type
    public void startAnimation(SharedAnimationBag animationBag, Set<View> viewList, ViewGroup viewGroup) {
        int animaType = animationBag.getAnimationType();
        if(animaType == SHARED_ELEMENTS) {
            sharedElementsAnimation(animationBag, viewList, viewGroup);
        }
    }


    private void sharedElementsAnimation(SharedAnimationBag animationBag, Set<View> sharedViews, ViewGroup container) {
        List<View> notSharedViews = getAllViewsForAlpha(container, sharedViews);
        TransitionDrawable drawable = (TransitionDrawable) container.getBackground();
        drawable.startTransition(800);
        for (View v : sharedViews) {
            SharedViewOldParameters sharedViewOldParameters = animationBag.getAnimationData()
                    .get(v.getTag());
            if (sharedViewOldParameters != null) {
                SharedViewAnimateParam animViewParam = new SharedViewAnimateParam();
                animateSharedViews(animViewParam, sharedViewOldParameters, v);
            }
        }
        animateNotSharedViews(notSharedViews);
    }

    //gets all views that needs alpha animation
    private List<View> getAllViewsForAlpha(View container, Set<View> sharedViews) {
        List<View> visited = new ArrayList<>();
        List<View> unvisited = new ArrayList<>();
        unvisited.add(container);

        while (!unvisited.isEmpty()) {
            View child = unvisited.remove(0);

            if(sharedViews.contains(child)) continue;

            if(child != container && child != container.findViewById(R.id.movingContainer)) {
                visited.add(child);
            }

            if (!(child instanceof ViewGroup)) continue;

            ViewGroup group = (ViewGroup) child;
            final int childCount = group.getChildCount();
            for (int i=0; i<childCount; i++) {
                unvisited.add(group.getChildAt(i));
            }
        }
        return visited;
    }

    //gets shared view position on screen and starts animation from old state.
    private void animateSharedViews(final SharedViewAnimateParam animViewParam, final SharedViewOldParameters ad, final View view) {
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                int[] screenLocation = new int[2];
                view.getLocationOnScreen(screenLocation);
                animViewParam.setLeftDelta(ad.getLeft() - screenLocation[0]);
                animViewParam.setTopDelta(ad.getTop() - screenLocation[1]);
                animViewParam.setScaleX((float) ad.getHeight() / view.getHeight());
                animViewParam.setScaleY((float) ad.getWidth() / view.getWidth());
                animViewParam.setView(view);
                startSharedTransition(animViewParam);
                return true;
            }
        });
    }

    //scales and translates shared views
    private void startSharedTransition(SharedViewAnimateParam animViewParam) {
        View v = animViewParam.getView();
        v.setAlpha(1f);
        v.setPivotY(0);
        v.setPivotX(0);
        v.setScaleX(animViewParam.getScaleX());
        v.setScaleY(animViewParam.getScaleY());
        v.setTranslationX(animViewParam.getLeftDelta());
        v.setTranslationY(animViewParam.getTopDelta());
        v.animate().setDuration(800).scaleX(1).scaleY(1).translationX(0).translationY(0).setInterpolator(new DecelerateInterpolator());
    }


    private void animateNotSharedViews(List<View> views) {
        for (View v : views) {
            if(v != null) {
                v.setAlpha(0f);
                v.animate().alpha(1f).setDuration(800).setInterpolator(new AccelerateInterpolator());
            }
        }
    }
}
