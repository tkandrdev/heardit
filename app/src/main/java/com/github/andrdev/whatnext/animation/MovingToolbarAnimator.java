package com.github.andrdev.whatnext.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;

import com.github.andrdev.whatnext.util.UiUtil;


//animates views on scroll
public class MovingToolbarAnimator {
    //stores view data
    MovingToolbarAnimationBag mAnimationBag;

    boolean isAnimating = false;

    private AnimatorSet hideAnimatorSet;

    private AnimatorSet showAnimatorSet;

    public MovingToolbarAnimator(MovingToolbarAnimationBag animationBag) {
        mAnimationBag = animationBag;
        createObjectAnimatorSets();
    }

    private void createObjectAnimatorSets() {
        createHideSet();
        createShowSet();
    }

    //creates AnimationSet for scroll down animation.
    private void createHideSet() {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(mAnimationBag.getMovingContainer(),
                "y", mAnimationBag.getMovingContainerNewTop());
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(mAnimationBag.getResizeView(),
                "y", mAnimationBag.getResizeViewNewTop());

        int stableHeight = mAnimationBag.getResizeViewHeight();

        //resizes resizeView from animationBag on each animation update.
        anim2.addUpdateListener(animation -> {
            int newHeight = stableHeight + mAnimationBag.getResizeViewOldTop()
                    - ((Float) animation.getAnimatedValue()).intValue();
            mAnimationBag.setResizeViewHeight(newHeight);
        });
        hideAnimatorSet = new AnimatorSet();

        hideAnimatorSet.playTogether(anim1, anim2);
        hideAnimatorSet.setDuration(300);

        hideAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    //creates AnimationSet for scroll up animation.
    private void createShowSet() {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(mAnimationBag.getMovingContainer(),
                "y", mAnimationBag.getMovingContainerOldTop());
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(mAnimationBag.getResizeView(),
                "y", mAnimationBag.getResizeViewOldTop());

        int stableHeight = mAnimationBag.getResizeViewNewHeight();

        //resizes resizeView from animationBag on each animation update.
        anim2.addUpdateListener(animation -> {
            int newHeight = stableHeight + mAnimationBag.getResizeViewNewTop()
                    - ((Float) animation.getAnimatedValue()).intValue();
            mAnimationBag.setResizeViewHeight(newHeight);
        });
        showAnimatorSet = new AnimatorSet();

        showAnimatorSet.playTogether(anim1, anim2);
        showAnimatorSet.setDuration(300);

        showAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                isAnimating = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    //checks for softkeyboard visibility and if currently animating
   private boolean canAnimate(Activity activity) {
        return !isAnimating  && !UiUtil.isSoftKeyboardVisible(activity);
    }


    public void scrollDetected(boolean isScrollDown, Activity activity) {
        if(!canAnimate(activity)) {
            return;
        } else if (isScrollDown) {
            moveToolbarUp();
        } else {
            moveToolbarDown();
        }
    }

    private void moveToolbarUp() {
        if (mAnimationBag.isContainerDown()) {
            hideAnimatorSet.start();
        }
    }

    private void moveToolbarDown() {
        if (!mAnimationBag.isContainerDown()) {
            showAnimatorSet.start();
        }
    }
}