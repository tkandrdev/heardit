package com.github.andrdev.whatnext.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.Season;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SeasonsRecyclerViewHolder extends BasicViewHolder<Season> {

    @InjectView(R.id.number)
     TextView mNumber;

    public SeasonsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    @Override
    void bindView(Context context, Season item) {
        mNumber.setText(item.getNumber());
    }
}
