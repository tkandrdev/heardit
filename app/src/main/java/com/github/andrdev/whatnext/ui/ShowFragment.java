package com.github.andrdev.whatnext.ui;

import android.os.Bundle;
import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Season;

import java.util.ArrayList;


public class ShowFragment extends BaseListFragment<Season> {

    @Override
    int getLayout() {
        return R.layout.fragment_rv;
    }

    @Override
    int getDataType() {
        return DataTypesConst.SEASONS;
    }

    @Override
    void onRowClick(View view, int position) {
        Season item = getDataList().get(position);
        Bundle bundle = getBundle();
        bundle.putParcelableArrayList(Constants.SEASON_LIST, new ArrayList<>(getDataList()));
        bundle.putParcelable(Constants.CURRENT_SEASON, item);
        bundle.putInt(Constants.SEASON_POSITION, position);
        bundle.putString(Constants.CURRENT_SEASON_NUMBER, item.getNumber());
        getActivityCallback().sendRequest(DataTypesConst.EPISODES, null, bundle);
    }
}
