package com.github.andrdev.whatnext.networking;

import android.app.Application;
import android.util.Base64;

import com.github.andrdev.whatnext.database.HeardItRoboSpiceDBHelper;
import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.Episodes;
import com.github.andrdev.whatnext.model.Movie;
import com.github.andrdev.whatnext.model.Movies;
import com.github.andrdev.whatnext.model.Season;
import com.github.andrdev.whatnext.model.Seasons;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.model.Shows;
import com.github.andrdev.whatnext.model.Song;
import com.github.andrdev.whatnext.model.Songs;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.ormlite.InDatabaseObjectPersisterFactory;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;
import com.squareup.okhttp.OkHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;


//base retrofit service with basic authentication
abstract class RetrofitBaseService extends RetrofitSpiceService {

    private static final String USERNAME = "";
    private static final String PASSWORD = "";

    private final static String BASE_URL = "https://www.tunefind.com/api/v1";

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }

    @Override
    protected Converter createConverter() {
        return new GsonConverter(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create());
    }
    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(TuneFindService.class);
    }

    protected RestAdapter.Builder createRestAdapterBuilder() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.setConnectTimeout(20000, TimeUnit.MILLISECONDS);
            RestAdapter.Builder builder = new RestAdapter.Builder()
                    .setEndpoint(BASE_URL)
                    .setConverter(getConverter())
                    .setClient(new OkClient(okClient));

                // concatenate username and password with colon for authentication
                final String credentials = USERNAME + ":" + PASSWORD;

                builder.setRequestInterceptor((request) -> {
                    // create Base64 encoded string
                    String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    request.addHeader("Authorization", string);
                    request.addHeader("Accept", "application/json");
                });
        return builder;
    }
}