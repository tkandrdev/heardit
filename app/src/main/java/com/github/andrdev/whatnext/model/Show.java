package com.github.andrdev.whatnext.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "show_list")
public class Show implements Parcelable, HeardItData {

    @DatabaseField(generatedId = true)
    private int id;

    @Expose
    @SerializedName("id")
    @DatabaseField
    private String link;

    @Expose
    @DatabaseField
    private String name;

    @Expose
    @DatabaseField
    private String thumb_url;

    @DatabaseField(foreign = true)
    private Shows shows;

    public Show() {
    }

    private Show(Parcel in) {
        link = in.readString();
        name = in.readString();
        thumb_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(link);
        dest.writeString(name);
        dest.writeString(thumb_url);
    }

    public static final Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>() {

        public Show createFromParcel(Parcel in) {
            return new Show(in);
        }

        public Show[] newArray(int size) {
            return new Show[size];
        }

    };

    public int getIdOrm() {
        return id;
    }

    public void setIdOrm(int id) {
        this.id = id;
    }

    public String getId() {
        return link;
    }

    public void setId(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public Shows getShows() {
        return shows;
    }

    public void setShows(Shows shows) {
        this.shows = shows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Show show = (Show) o;

        if (id != show.id) return false;
        if (link != null ? !link.equals(show.link) : show.link != null) return false;
        if (name != null ? !name.equals(show.name) : show.name != null) return false;
        if (thumb_url != null ? !thumb_url.equals(show.thumb_url) : show.thumb_url != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (thumb_url != null ? thumb_url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Show{" +
                "id=" + id +
                ", link='" + link + '\'' +
                ", name='" + name + '\'' +
                ", thumb_url='" + thumb_url + '\'' +
                ", shows=" + shows +
                '}';
    }
}
