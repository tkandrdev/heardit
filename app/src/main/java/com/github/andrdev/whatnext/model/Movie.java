package com.github.andrdev.whatnext.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "movie_list")
public class Movie implements Parcelable, HeardItData {

    @DatabaseField(generatedId = true)
    private int id;

    @Expose
    @SerializedName("id")
    @DatabaseField
    private String link;

    @Expose
    @DatabaseField
    private String name;

    @Expose
    @DatabaseField
    private String thumb_url;

    @DatabaseField(foreign = true)
    private Movies movies;

    public Movie() {
    }

    private Movie(Parcel in) {
        link = in.readString();
        name = in.readString();
        thumb_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(link);
        dest.writeString(name);
        dest.writeString(thumb_url);
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {

        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }

    };

    public int getIdOrm() {
        return id;
    }

    public void setIdOrm(int id) {
        this.id = id;
    }

    public String getId() {
        return link;
    }

    public void setId(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public Movies getMovies() {
        return movies;
    }

    public void setMovies(Movies movies) {
        this.movies = movies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (id != movie.id) return false;
        if (link != null ? !link.equals(movie.link) : movie.link != null) return false;
        if (name != null ? !name.equals(movie.name) : movie.name != null) return false;
        if (thumb_url != null ? !thumb_url.equals(movie.thumb_url) : movie.thumb_url != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (thumb_url != null ? thumb_url.hashCode() : 0);
        return result;
    }
}
