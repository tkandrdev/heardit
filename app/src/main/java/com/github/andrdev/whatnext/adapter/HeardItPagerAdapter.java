package com.github.andrdev.whatnext.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class HeardItPagerAdapter extends FragmentPagerAdapter {


    private final TabHelper mTabHelper;

    public HeardItPagerAdapter(FragmentManager fm, String pager) {
        super(fm);
        mTabHelper = TabFabrica.giveHelper(pager);
    }

    public HeardItPagerAdapter(FragmentManager fm, TabHelper tabHelper) {
        super(fm);
        this.mTabHelper = tabHelper;
    }

    @Override
    public Fragment getItem(int position) {
        return mTabHelper.getFragment(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabHelper.getTitle(position);
    }

    @Override
    public int getCount() {
        return mTabHelper.getCount();
    }

}
