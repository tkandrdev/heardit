package com.github.andrdev.whatnext.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class Season implements Parcelable, HeardItData {


    private String id;

    @Expose
    private String number;

    @Expose
    private String song_count;

    public Season() {
    }

    private Season(Parcel in) {
        id = in.readString();
        number = in.readString();
        song_count = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(number);
        dest.writeString(song_count);
    }

    public static final Parcelable.Creator<Season> CREATOR = new Parcelable.Creator<Season>() {

        public Season createFromParcel(Parcel in) {
            return new Season(in);
        }

        public Season[] newArray(int size) {
            return new Season[size];
        }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Season season = (Season) o;

        if (song_count != null ? !song_count.equals(season.song_count) : season.song_count != null)
            return false;
        if (id != null ? !id.equals(season.id) : season.id != null) return false;
        if (number != null ? !number.equals(season.number) : season.number != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (song_count != null ? song_count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Season{" +
                "id='" + id + '\'' +
                ", number='" + number + '\'' +
                ", episode_count='" + song_count + '\'' +
                '}';
    }
}
