package com.github.andrdev.whatnext.adapter;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;

import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.HeardItData;
import com.github.andrdev.whatnext.model.Season;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.ui.Constants;
import com.github.andrdev.whatnext.ui.EpisodeFragment;
import com.github.andrdev.whatnext.ui.SeasonFragment;

import java.util.List;



public class FragmentTabHelper<T extends HeardItData & Parcelable> extends TabHelper {

    private List<T> mDataList;
    private final Show mShow;

    public FragmentTabHelper(Bundle bundle) {
        mDataList = bundle.getParcelableArrayList(Constants.EPISODE_LIST);
        if (mDataList == null) {
            mDataList = bundle.getParcelableArrayList(Constants.SEASON_LIST);
        }
        mShow = bundle.getParcelable(Constants.CURRENT_SHOW);
    }

    @Override
    int getCount() {
        return mDataList.size() == 0 ? 0 : mDataList.size();
    }

    @Override
    Fragment getFragment(int position) {
        Bundle bundle = new Bundle();
        Fragment fragment;

        bundle.putParcelable(Constants.CURRENT_SHOW, mShow);

        T item = mDataList.get(position);

        if (item instanceof Season) {
            fragment = new SeasonFragment();
            bundle.putParcelable(Constants.CURRENT_SEASON, item);
        } else if (item instanceof Episode) {
            fragment = new EpisodeFragment();
            bundle.putParcelable(Constants.CURRENT_EPISODE, item);
        } else {
            return null;
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    String getTitle(int position) {
        String title;
        T item = mDataList.get(position);
        if (item instanceof Episode) {
            title = ((Episode) item).getNumber();
        } else {
            title = ((Season) item).getNumber();
        }
        return title;
    }
}
