package com.github.andrdev.whatnext.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class Episode implements Parcelable, HeardItData {

    @Expose
    private String id;

    @Expose
    private String name;

    @Expose
    private String number;

    @Expose
    private String song_count;

    public Episode() {

    }

    private Episode(Parcel in) {
        id = in.readString();
        name = in.readString();
        number = in.readString();
        song_count = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(song_count);
    }

    public static final Parcelable.Creator<Episode> CREATOR = new Parcelable.Creator<Episode>() {

        public Episode createFromParcel(Parcel in) {
            return new Episode(in);
        }

        public Episode[] newArray(int size) {
            return new Episode[size];
        }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSong_count() {
        return song_count;
    }

    public void setSong_count(String song_count) {
        this.song_count = song_count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Episode episode = (Episode) o;

        if (id != null ? !id.equals(episode.id) : episode.id != null) return false;
        if (name != null ? !name.equals(episode.name) : episode.name != null) return false;
        if (number != null ? !number.equals(episode.number) : episode.number != null) return false;
        if (song_count != null ? !song_count.equals(episode.song_count) : episode.song_count != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (song_count != null ? song_count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Episode{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", song_count='" + song_count + '\'' +
                '}';
    }
}
