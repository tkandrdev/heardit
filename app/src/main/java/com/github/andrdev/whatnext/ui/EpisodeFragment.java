package com.github.andrdev.whatnext.ui;

import android.view.View;

import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.Song;


public class EpisodeFragment extends BaseListFragment<Song> {

    @Override
    boolean sendOnStartRequest() {
        Episode mEpisode = getBundle().getParcelable(Constants.CURRENT_EPISODE);
        if(mEpisode != null) {
            return super.sendOnStartRequest();
        } else{
            return false;
        }
    }

    @Override
    int getDataType() {
        return DataTypesConst.SONGS;
    }

    @Override
    int getLayout() {
        return R.layout.fragment_rv;
    }

    @Override
    void onRowClick(View view, int position) {
    }
}

