package com.github.andrdev.whatnext.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;


public class Song implements HeardItData, Parcelable {

    @Expose
    private String id;

    @Expose
    private String name;

    @Expose
    private String scene;

    @Expose
    private Artist artist;

    public Song() {
    }

    private Song(Parcel in) {
        id = in.readString();
        name = in.readString();
        scene = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(scene);
    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {

        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }
}
