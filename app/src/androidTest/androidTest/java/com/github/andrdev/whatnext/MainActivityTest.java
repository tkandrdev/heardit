package com.github.andrdev.whatnext;

import android.app.Instrumentation;
import android.support.v4.view.ViewPager;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.view.View;

import com.github.andrdev.whatnext.ui.MainActivity;

/**
 * Created by taiyokaze on 3/10/15.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;
    private Instrumentation mInstrumentation;
    private View mSearchButton;

    public MainActivityTest(Class<MainActivity> activityClass) {
        super(activityClass);
    }
    public MainActivityTest() {
        super(MainActivity.class);
    }
//    @Override
//    protected void setUp() throws Exception {
//        super.setUp();
//        mInstrumentation = getInstrumentation();
//        mActivity = getActivity();
//        mSearchButton = mActivity.findViewById(R.id.menu_search);
//    }
//
//    public void testViewPager() {
//        ViewPager pager = (ViewPager) mActivity.findViewById(R.id.viewpager);
//        assertNotNull(pager);
//        assertEquals(pager.getCurrentItem(), 0);
//    }
//
//    public void testSerchButton() {
//        View mainActivityDecorView = mActivity.getWindow().getDecorView();
//        ViewAsserts.assertOnScreen(mainActivityDecorView, mSearchButton);
//    }
}
