package com.github.andrdev.whatnext;

import android.os.Bundle;

import com.github.andrdev.whatnext.adapter.FragmentTabHelper;
import com.github.andrdev.whatnext.model.Episode;
import com.github.andrdev.whatnext.model.Season;
import com.github.andrdev.whatnext.model.Show;
import com.github.andrdev.whatnext.ui.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by taiyokaze on 4/9/15.
 */
public class FragmentTabHelperTest {

    ArrayList<Episode> episodeList = new ArrayList<>();
    ArrayList<Season> seasonList = new ArrayList<>();
    String[] episodeName = {"Truth Be Told", "So It Begins", "Parity", "A Broken Heart", "Mea Culpa",
            "Spirit", "The Confession", "The Box", "The Coup", "Page 47"};
    String[] itemNumber = {"1", "2", "3", "5", "9", "10", "11", "12", "13", "14"};
    String[] itemSongCount = {"3", "5", "4", "2", "5", "6", "6", "3", "11", "3"};
    FragmentTabHelper episodeFragTabHelper;
    FragmentTabHelper seasonFragTabHelper;

    private void populateEpisodeList() {
        for(int i = 0; i < episodeName.length; i++) {
            Episode episode = new Episode();
            episode.setName(episodeName[i]);
            episode.setNumber(itemNumber[i]);
            episode.setSong_count(itemSongCount[i]);
        }
    }

    private void populateSeasonList() {
        for(int i = 0; i < episodeName.length; i++) {
            Season episode = new Season();
            episode.setNumber(itemNumber[i]);
            episode.setSong_count(itemSongCount[i]);
        }
    }

    void setUp() {
        populateEpisodeList();
        populateSeasonList();
        Bundle episodeBundle = new Bundle();
        Bundle seasonBundle = new Bundle();
        Show show = new Show();
        seasonBundle.putParcelable(Constants.CURRENT_SHOW, show);
        seasonBundle.putParcelableArrayList(Constants.SEASON_LIST, seasonList);
        seasonFragTabHelper = new FragmentTabHelper(seasonBundle);
        episodeBundle.putParcelable(Constants.CURRENT_SHOW, show);
        episodeBundle.putParcelableArrayList(Constants.EPISODE_LIST, episodeList);
        episodeFragTabHelper = new FragmentTabHelper(episodeBundle);
    }

}
