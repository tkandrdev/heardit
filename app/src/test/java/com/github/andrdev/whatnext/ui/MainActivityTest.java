package com.github.andrdev.whatnext.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

import com.github.andrdev.whatnext.BuildConfig;
import com.github.andrdev.whatnext.R;
import com.github.andrdev.whatnext.animation.SharedAnimationBag;
import com.github.andrdev.whatnext.animation.SharedViewOldParameters;
import com.github.andrdev.whatnext.model.DataTypesConst;
import com.github.andrdev.whatnext.ui.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricGradleTestRunner.class)
@Config(manifest = "app/src/main/AndroidManifest.xml", constants = BuildConfig.class, emulateSdk = 18)
public class MainActivityTest {
    MainActivity mActivity;
    Menu mMenu;
    ViewPager mViewPager;

    @Before
    public void setUp() {
        mActivity = Robolectric.setupActivity(MainActivity.class);
        mMenu = shadowOf(mActivity).getOptionsMenu();
        mViewPager = (ViewPager) shadowOf(mActivity).findViewById(R.id.viewpager);
    }

    @Test
    public void shouldNotBeNull() {
        assertThat(mActivity).isNotNull();
        assertThat(mMenu).isNotNull();
        assertThat(mViewPager).isNotNull();
    }

    @Test
    public void onCreate_shouldInflateTheMenu() throws Exception {
        assertThat(mMenu.findItem(R.id.menu_search).getTitle()).isEqualTo("Search");
    }

    @Test
    public void viewPagerStartPage() {
        assertThat(mViewPager.getCurrentItem()).isEqualTo(0);
    }

    @Test
    public void nextActivityShow() {
        Bundle bundle = new Bundle();
        mActivity.sendRequest(DataTypesConst.SEASONS, null, bundle);
        Intent intent = shadowOf(mActivity).peekNextStartedActivity();
        assertThat(shadowOf(intent).getIntentClass()).isEqualTo(ShowActivity.class);
    }

    @Test
    public void nextActivityMovie(){
        Bundle bundle = new Bundle();
        mActivity.sendRequest(DataTypesConst.SONGS, null, bundle);
        Intent intent = shadowOf(mActivity).peekNextStartedActivity();
        assertThat(shadowOf(intent).getIntentClass()).isEqualTo(MovieActivity.class);
    }

    @After
    public void tearDown() {
        mActivity = null;
        mMenu = null;
        mViewPager = null;
    }
}
